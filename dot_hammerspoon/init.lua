-- Load SpoonInstall
hs.loadSpoon("SpoonInstall")
spoon.SpoonInstall.use_syncinstall = true
Install = spoon.SpoonInstall

Install:andUse("RoundedCorners", {start = true})
Install:andUse("Caffeine", {start = true})
spoon.Caffeine:setState(true)

-- Hyper
-- A global variable for the Hyper Mode
hyper = hs.hotkey.modal.new({}, 'F17')

-- Enter Hyper Mode when F18 (Hyper/Capslock) is pressed
function enterHyperMode()
    hyper.triggered = false
    hyper:enter()
end

-- Leave Hyper Mode when F18 (Hyper/Capslock) is pressed,
-- send ESCAPE if no other keys are pressed.
function exitHyperMode()
    hyper:exit()
    if not hyper.triggered then hs.eventtap.keyStroke({}, 'ESCAPE') end
end

-- Bind the Hyper key
f18 = hs.hotkey.bind({}, 'F18', enterHyperMode, exitHyperMode)

-- Hyper bindings
hyper:bind({}, 'return', function()
    hs.application.launchOrFocus("VSCodium")
    hyper.triggered = true
end)

hyper:bind({'shift'}, 'return', function()
    hs.application.launchOrFocus("Kitty")
    hyper.triggered = true
end)

hyper:bind({'shift'}, 'f', function()
    hs.execute('open -n -a "Firefox"')
    hyper.triggered = true
end)

hyper:bind({}, 'e', function()
    hs.application.launchOrFocus("Finder")
    hs.appfinder.appFromName("Finder"):activate()
    hyper.triggered = true
end)

-- Switch spaces
for space = 1, 9 do
    hyper:bind({}, tostring(space), function()
        hs.eventtap.keyStroke({'cmd', 'alt', 'shift', 'ctrl'}, tostring(space))
        hyper.triggered = true
    end)
end

