#!/bin/zsh

# Navigation
setopt AUTO_CD

setopt AUTO_PUSHD
setopt PUSHD_IGNORE_DUPS
setopt PUSHD_SILENT

setopt CORRECT
setopt CDABLE_VARS
setopt EXTENDED_GLOB

#autoload -Uz bd; bd

# History
setopt EXTENDED_HISTORY
setopt SHARE_HISTORY
setopt HIST_EXPIRE_DUPS_FIRST
setopt HIST_IGNORE_DUPS
setopt HIST_IGNORE_ALL_DUPS
setopt HIST_FIND_NO_DUPS
setopt HIST_IGNORE_SPACE
setopt HIST_SAVE_NO_DUPS
setopt HIST_VERIFY

# bindkey '^[[A' history-substring-search-up
# bindkey '^[[B' history-substring-search-down
bindkey -M vicmd 'k' history-substring-search-up
bindkey -M vicmd 'j' history-substring-search-down

# Aliases
alias g='git'
alias nvim='CC=/opt/homebrew/bin/gcc-11 nvim'

# Zplug
source $ZPLUG_HOME/init.zsh

zplug "mafredri/zsh-async"
zplug "zsh-users/zsh-history-substring-search"
zplug "zdharma-continuum/fast-syntax-highlighting", defer:2
zplug "ael-code/zsh-colored-man-pages"
zplug "modules/ssh", from:prezto
zplug "modules/archive", from:prezto
zplug "sindresorhus/pure", use:pure.zsh, as:theme
zplug "zsh-users/zsh-completions"

if ! zplug check --verbose; then
  printf "Install? [y/N]: "
  if read -q; then
    echo; zplug install
  fi
fi

zplug load

# SSH
zstyle ':prezto:module:ssh:load' identities 'personal' 'telnyx'

# ASDF
. /opt/homebrew/opt/asdf/libexec/asdf.sh

# Doom
export PATH="$HOME/.config/emacs/bin:$PATH"

# GPG
export GPG_TTY=$(tty)

# Deno
export PATH="$HOME/.deno/bin:$PATH"

# GNU utils
alias find='gfind'
alias grep='ggrep'

# vim mode
bindkey -v
export KEYTIMEOUT=1

# arlocal functions
function arlocal-info {
  curl http://127.0.0.1:1984/info
}

function arlocal-fund {
  curl http://127.0.0.1:1984/mint/$1/$2
}

function arlocal-balance {
  curl http://127.0.0.1:1984/wallet/$1/balance
}

function arlocal-mine {
  curl http://127.0.0.1:1984/mine/$1
}
