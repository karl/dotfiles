local fn = vim.fn
local o = vim.o

-- Packer install
local install_path = fn.stdpath('data')..'/site/pack/packer/start/packer.nvim'
if fn.empty(fn.glob(install_path)) > 0 then
  o.runtimepath = fn.stdpath('data') .. '/site/pack/*/start/*,' .. o.runtimepath
  packer_bootstrap = fn.system({'git', 'clone', '--depth', '1', 'https://github.com/wbthomason/packer.nvim', install_path})
end

function get_setup(name)
  return string.format('require("setup/%s")', name)
end

-- Plugins
require('packer').startup({function(use)
  use 'wbthomason/packer.nvim'

  use {
    'EdenEast/nightfox.nvim',
    config = get_setup('nightfox')
  }

  use {
    'nvim-treesitter/nvim-treesitter',
    run = ':TSUpdate',
    config = get_setup('treesitter')
  }

  use {
    'neovim/nvim-lspconfig',
    config = get_setup('lspconfig')
  }

  use {
    'hrsh7th/nvim-cmp',
    requires = {
      'hrsh7th/cmp-nvim-lsp',
      'saadparwaiz1/cmp_luasnip',
      'hrsh7th/cmp-path',
      {'petertriho/cmp-git', requires = 'nvim-lua/plenary.nvim'}
    },
    config = get_setup('cmp')
  }

  use 'L3MON4D3/LuaSnip'

  use {
    'numToStr/Comment.nvim',
    config = get_setup('comment')
  }

  use {
    'nvim-lualine/lualine.nvim',
    requires = {'kyazdani42/nvim-web-devicons', opt = true},
    config = get_setup('lualine')
  }

  use {
    'kyazdani42/nvim-tree.lua',
    requires = {'kyazdani42/nvim-web-devicons'},
    config = get_setup('nvim-tree')
  }

  use {
    'lewis6991/gitsigns.nvim',
    requires = {'nvim-lua/plenary.nvim'},
    config = function() require('gitsigns').setup() end
  }

  use {
    'TimUntersberger/neogit',
    requires = 'nvim-lua/plenary.nvim',
    config = function() require('neogit').setup({}) end,
    cmd = {'Neogit'}
  }

  use {
    'folke/which-key.nvim',
    config = get_setup('which-key')
  }

  use {
    'nvim-telescope/telescope.nvim',
    requires = {'nvim-lua/plenary.nvim', 'kyazdani42/nvim-web-devicons'},
    config = get_setup('telescope')
  }
  use {'nvim-telescope/telescope-fzf-native.nvim', run = 'make' }

  use {
    'pwntester/octo.nvim',
    requires = {
      'nvim-lua/plenary.nvim',
      'nvim-telescope/telescope.nvim',
      'kyazdani42/nvim-web-devicons',
    },
    config = function ()
      require"octo".setup()
    end
  }

  if packer_bootstrap then
    require('packer').sync()
  end
end,
config = {
  display = {
    open_fn = function()
      return require('packer.util').float({ border = 'single' })
    end
  },
  profile = {
    enable = true,
    threshold = 1
  }
}})

vim.cmd([[
  augroup packer_user_config
    autocmd!
    autocmd BufWritePost plugins.lua source <afile> | PackerCompile
  augroup end
]])
