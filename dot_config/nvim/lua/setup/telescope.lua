local actions = require('telescope.actions')

require('telescope').setup{
  defaults = {
    width = 0.8;
    color_devicons = true,
    prompt_prefix = '❯ ';
    selection_caret = '  ';
    borderchars = {
      prompt = { '─', '│', '─', '│', '┌', '┐', '┘', '└'},
      results = {"─", "│", "─", "│", "┌", "┐", "┘", "└"},
      preview = { '─', '│', '─', '│', '┌', '┐', '┘', '└'},
    },
    mappings = {
      i = {
        ['<esc>'] = actions.close
      },
    },
  },
  pickers = {
    buffers = {
      prompt_title = false,
      results_title = false,
      preview_title = false,
    },
    git_files = {
      prompt_title = false,
      results_title = false,
      preview_title = false,
    },
    find_files = {
      prompt_title = false,
      results_title = false,
      preview_title = false,
    },
    live_grep = {
      prompt_title = false,
      results_title = false,
      preview_title = false,
    }
  },
  extensions = {
    fzf = {
      fuzzy = true,                    -- false will only do exact matching
      override_generic_sorter = true,  -- override the generic sorter
      override_file_sorter = true,     -- override the file sorter
      case_mode = "smart_case",        -- or "ignore_case" or "respect_case"
                                       -- the default case_mode is "smart_case"
    }
  }
}

require('telescope').load_extension('fzf')
