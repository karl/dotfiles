require('nightfox').setup({
  options = {
    terminal_colors = true,
    styles = {
      comments = 'italic',
      functions = 'italic',
      keywords = 'italic,bold',
      numbers = 'NONE',
      strings = 'NONE',
      types = 'bold',
      variables = 'NONE',
    },
    inverse = {
      match_paren = false,
      visual = false,
      search = false,
    },
    modules = {
      treesitter = true,
      cmp = true,
      gitsigns = true,
      native_lsp = true,
      neogit = true,
      nvimtree = true,
      telescope = true
    },
  }
})

vim.cmd('colorscheme nightfox')
