require'nvim-treesitter.configs'.setup {
  ensure_installed = {
    'bash',
    'comment',
    'css',
    'dart',
    'dockerfile',
    'graphql',
    'html',
    'javascript',
    'json',
    'lua',
    'make',
    'markdown',
    'toml',
    'typescript',
    'yaml'
  },
  sync_install = false,
  highlight = {
    enable = true,
    disable = {},
  }
}
